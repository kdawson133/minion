#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright (c) 2015 Kirk Dawson. This program is open-source software,
# and may be redistributed under the terms of the MIT license. See the
# LICENSE file in this distribution for details.
#
# minion is a simple static site generator
# that utilises multimarkdown to render
# HTML pages. Version 1.2
#
# Imports
import minmod, argparse

#

def main():
    minmod.setcolor()
    minmod.dependencycheck()

    parser = argparse.ArgumentParser(prog="minion", description="An automated website compiler using multimarkdown", epilog="minion " + u"\u00A9" + "2015 Kirk Dawson")
    parser.add_argument('-v','--version', action='version', version='minion 1.2')
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-i", "--init", action="store_true", help="initialise minion in current directory")
    group.add_argument("-e", "--encode", action="store_true", help="batch encode images and move to includes")
    group.add_argument("-b", "--build", action="store_true", help="build current minion site")
    group.add_argument("-s", "--serve", action="store_true", help="serve current build on port:1313")
    group.add_argument("-p", "--publish", action="store_true", help="publish current build")
    args = parser.parse_args()

    if args.init:
        minmod.initminion();
    elif args.encode:
        minmod.batchencode();
    elif args.build:
        minmod.buildminion();
    elif args.serve:
        minmod.servesite();
    elif args.publish:
        minmod.publish();
    else:
        print "usage: minion minion [-h] [-i] [-b] [-s] [-p]"
        print "minion: error: no argument supplied."
    return

if __name__ == "__main__":
    main()
