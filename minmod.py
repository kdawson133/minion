#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Copyright (c) 2015 Kirk Dawson. This program is open-source software,
# and may be redistributed under the terms of the MIT license. See the
# LICENSE file in this distribution for details.
#
# Library for minion
#
# imports
import os, shutil, ConfigParser, time, timeit, base64
#
# minion functions
def setcolor():
    global HEADER
    HEADER = '\033[95m'
    global OKBLUE
    OKBLUE = '\033[34m'
    global OKGREEN
    OKGREEN = '\033[32m'
    global WARNING
    WARNING = '\033[35m'
    global FAIL
    FAIL = '\033[31m'
    global ENDC
    ENDC = '\033[0m'
    global BOLD
    BOLD = '\033[1m'
    global UNDERLINE
    UNDERLINE = '\033[4m'
    return
#
def which(program):
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None
#
def dependencycheck():
    # Check if dependacies have been installed
    dep_1 = 0
    dep_2 = 0
    dep_3 = 0
    dep_4 = 0
    dep_5 = 0
    dep_6 = 0
    if which("yuicompressor") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency yuicompressor not installed!"
        print "See https://github.com/yui/yuicompressor/releases"
        dep_1 = 1
    if which("multimarkdown") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency multimarkdown not installed!"
        print "See http://fletcherpenney.net/multimarkdown/download/"
        dep_2 = 1
    if which("http-server") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency http-server not installed!"
        print "See https://www.npmjs.com/package/http-server"
        dep_3 == 1
    if which("base64") is None:
        print FAIL + "ERROR: " + ENDC + "Dependency base64 not installed!"
        print "http://base64.sourceforge.net/"
        if which("rsync") is None:
            print FAIL + "ERROR: " + ENDC + "Dependency rsync not installed!"
            print "See https://rsync.samba.org/"
        dep_3 = 1
    if (dep_1 or dep_2 or dep_3) == 1:
        exit(2)
    return
#
def initminion():
    # itialise minion and build structure
    print OKBLUE + "Initialsing minion..." + ENDC
    print ""
    if not os.path.exists("content"):
        os.makedirs("content")
        print "--> content directory created for MultiMarkdown files"
        print ""
    if not os.path.exists("media"):
        os.makedirs("media")
        print "--> media directory created for Image files"
        print ""
    if not os.path.exists("encodes"):
        os.makedirs("encodes")
        print "--> encodes directory created for images to be encoded"
        print ""
    if not os.path.exists("includes"):
        os.makedirs("includes")
        print "--> includes directory created for included Text files"
        print ""
    if not os.path.exists("public"):
        os.makedirs("public")
        print "--> public directory created for CSS, fonts and .js files"
        print ""
    print OKBLUE + "Initialisation Complete" + ENDC
    print ""
    return
#
def batchencode():
    # start timing
    betic = timeit.default_timer()
    # Check if minion has been intialised
    if not os.path.exists("content"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("includes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("media"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("public"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("encodes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    print ""
    print OKBLUE + "minion is Commencing Batch Encode..." + ENDC
    print ""
    # Check if there is any images to encode
    print "Searching for Images to Encode..."
    pathtoimages = os.path.dirname(os.path.realpath("encodes/"))
    pngimgcount = 0
    jpgimgcount = 0
    gifimgcount = 0
    for root, directories, filenames in os.walk("encodes/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if walkfilename.endswith('.png'):
                pngimgcount = pngimgcount + 1
            if walkfilename.endswith('.jpg'):
                jpgimgcount = jpgimgcount + 1
            if walkfilename.endswith('.gif'):
                gifimgcount = gifimgcount + 1
    imgcount = pngimgcount + jpgimgcount + gifimgcount
    print "-->", imgcount, " Image Files Found"
    print ""
    if imgcount == 0:
        print FAIL + "No Images to Encode!" + ENDC
        print ""
        exit(2)

    # Converting jpg to base 64
    if jpgimgcount >= 1:
        print "Converting .jpg to .jpg.txt..."
        pathtoencodes = os.path.dirname(os.path.realpath("encodes/"))
        for root, directories, filenames in os.walk("encodes/"):
            for filename in filenames:
                walkfilename = pathtoencodes + "/" + os.path.join(root,filename)
                if walkfilename.endswith('.jpg'):
                    newext = ".jpg.txt"
                    jpgtxtfilename = pathtoencodes + "/includes/" + os.path.splitext(filename)[0] + newext
                    print OKGREEN + filename + ENDC
                    jpgcaption = raw_input('Enter Caption (enter for none): ')
                    print "--> Encoding: " + pathtoencodes + walkfilename
                    print "--------> To: " + pathtoencodes + jpgtxtfilename
                    jpgline_1 = "echo " + "\"![" + jpgcaption + "](data:image/jpg;base64,\c\" > " + jpgtxtfilename
                    jpgline_2 = "base64 " + walkfilename + " >> " + jpgtxtfilename
                    jpgline_3 = "echo \")\" >> " + jpgtxtfilename
                    os.system(jpgline_1)
                    os.system(jpgline_2)
                    os.system(jpgline_3)
                    os.remove(walkfilename)
                    print ""
	# Converting png to base 64
    if pngimgcount >= 1:
        print "Converting .png to .png.txt..."
        pathtoencodes = os.path.dirname(os.path.realpath("encodes/"))
        for root, directories, filenames in os.walk("encodes/"):
            for filename in filenames:
                walkfilename = pathtoencodes + "/" + os.path.join(root,filename)
                if walkfilename.endswith('.png'):
                    newext = ".png.txt"
                    pngtxtfilename = pathtoencodes + "/includes/" + os.path.splitext(filename)[0] + newext
                    print OKGREEN + filename + ENDC
                    pngcaption = raw_input('Enter Caption (enter for none): ')
                    print "--> Encoding: " + pathtoencodes + walkfilename
                    print "--------> To: " + pathtoencodes + pngtxtfilename
                    pngline_1 = "echo " + "\"![" + pngcaption + "](data:image/png;base64,\c\" > " + pngtxtfilename
                    pngline_2 = "base64 " + walkfilename + " >> " + pngtxtfilename
                    pngline_3 = "echo \")\" >> " + pngtxtfilename
                    os.system(pngline_1)
                    os.system(pngline_2)
                    os.system(pngline_3)
                    os.remove(walkfilename)
                    print ""
	# Converting gif to base 64
    if gifimgcount >= 1:
        print "Converting .gif to .gif.txt..."
        pathtoencodes = os.path.dirname(os.path.realpath("encodes/"))
        for root, directories, filenames in os.walk("encodes/"):
            for filename in filenames:
                walkfilename = pathtoencodes + "/" + os.path.join(root,filename)
                if walkfilename.endswith('.gif'):
                    newext = ".gif.txt"
                    giftxtfilename = pathtoencodes + "/includes/" + os.path.splitext(filename)[0] + newext
                    print OKGREEN + filename + ENDC
                    gifcaption = raw_input('Enter Caption (enter for none): ')
                    print "--> Encoding: " + pathtoencodes + walkfilename
                    print "--------> To: " + pathtoencodes + giftxtfilename
                    gifline_1 = "echo " + "\"![" + gifcaption + "](data:image/gif;base64,\c\" > " + giftxtfilename
                    gifline_2 = "base64 " + walkfilename + " >> " + giftxtfilename
                    gifline_3 = "echo \")\" >> " + giftxtfilename
                    os.system(gifline_1)
                    os.system(gifline_2)
                    os.system(gifline_3)
                    os.remove(walkfilename)
                    print ""

    betoc = timeit.default_timer()
    bett = betoc - betic
    print ""
    print OKBLUE + "Batch Encode Complete" + ENDC
    print ("Time taken: %.3f seconds" % bett)
    print ""
    return
#
def buildminion():
    # Start timing
    tic = timeit.default_timer()
    # Check if minion has been intialised
    if not os.path.exists("content"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("includes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("media"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("public"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)
    if not os.path.exists("encodes"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be initialised!"
        print ""
        exit(2)

    # Starting Build
    print ""
    print OKBLUE + "minion is Commencing Build..." + ENDC
    print ""
    # clean previous build if present
    print "Cleaning Previous Builds..."
    print "--> CLEAN"
    print ""
    if os.path.exists("out/"):
        shutil.rmtree("out/", ignore_errors=True)

    # look for content
    print "Searching for Content..."
    pathtocontent = os.path.dirname(os.path.realpath("content/"))
    mmdcount = 0
    for root, directories, filenames in os.walk("content/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if walkfilename.endswith('.mmd'):
                mmdcount = mmdcount + 1
    print "-->", mmdcount, " MultiMarkdown Files Found"
    print ""
    if mmdcount == 0:
        print FAIL + "No Content to Build!" + ENDC
        print ""
        exit(2)

    # Exporting Content
    print "Exporting Content..."
    if not os.path.exists("out/"):
        shutil.copytree("content/","out/", ignore = shutil.ignore_patterns('.*'))
        print "--> EXPORTED"
        print ""
    else:
        shutil.rmtree("out/")
        print WARNING + "WARNING: " +ENDC + "Content being exported again!"
        time.sleep(2)
        shutil.copytree("content/","out/", ignore = shutil.ignore_patterns('.*'))
        print "--> EXPORTED"
        print ""

    #Look for CSS
    print "Searching for CSS..."
    listing = os.listdir("public")
    csscount = 0
    for files in listing:
        if (files.endswith('.css' ) and (not files.endswith('.min.css' ))):
            csscount = csscount + 1
    if csscount > 0:
        print "-->", csscount, "CSS Files Found"
    if csscount == 0:
        print WARNING + "WARNING: " + ENDC + "No CSS Found"
    print ""

    #Look for JS
    print "Searching for JS..."
    listing = os.listdir("public")
    jscount = 0
    for files in listing:
        if (files.endswith('.js') and (not files.endswith('.min.js'))):
            jscount = jscount + 1
    if jscount > 0:
        print "-->", jscount, "JS Files Found"
    if jscount == 0:
        print WARNING + "WARNING: " + ENDC + "No JS Found"
    print ""

    # copy public to out
    print "Exporting Public Files..."
    shutil.copytree("public","out/public", ignore = shutil.ignore_patterns('.*'))
    print "--> EXPORTED"
    print ""

    # copy media to out
    print "Exporting Media Files..."
    shutil.copytree("media","out/media", ignore = shutil.ignore_patterns('.*'))
    print "--> EXPORTED"
    print ""

    # Convert minified css
    if csscount >= 1:
        print "Converting css to min.css..."
        pathtooutpublic = os.path.dirname(os.path.realpath("out/public/"))
        for root, directories, filenames in os.walk("out/public/"):
            for filename in filenames:
                walkfilename = os.path.join(root,filename)
                if (walkfilename.endswith('.css' ) and (not walkfilename.endswith('.min.css' ))):
                    newext = ".min.css"
                    cssminfilename = os.path.splitext(walkfilename)[0] + newext
                    print "--> Compressing: " + pathtooutpublic + "/" + walkfilename
                    print "-----------> To: " + pathtooutpublic + "/" + cssminfilename
                    yuicmd = "yuicompressor " + walkfilename + " > " + cssminfilename
                    os.system(yuicmd)
                    os.remove(walkfilename)
                    print ""

    # Convert minified js
    if jscount >= 1:
        print "Converting js to min.jss..."
        pathtooutpublic = os.path.dirname(os.path.realpath("out/public/"))
        for root, directories, filenames in os.walk("out/public/"):
            for filename in filenames:
                walkfilename = os.path.join(root,filename)
                if (walkfilename.endswith('.js' ) and (not walkfilename.endswith('.min.js' ))):
                    newext = ".min.js"
                    jsminfilename = os.path.splitext(walkfilename)[0] + newext
                    print "--> Compressing: " + pathtooutpublic + "/" + walkfilename
                    print "-----------> To: " + pathtooutpublic + "/" + jsminfilename
                    yuicmd = "yuicompressor " + walkfilename + " > " + jsminfilename
                    os.system(yuicmd)
                    os.remove(walkfilename)
                    print ""

    # Convert markdown to html
    print "Converting MultiMarkdown to HTML..."
    pathtoout = os.path.dirname(os.path.realpath("out/"))
    for root, directories, filenames in os.walk("out/"):
        for filename in filenames:
            walkfilename = os.path.join(root,filename)
            if walkfilename.endswith('.mmd'):
                newext = ".html"
                htmlfilename = os.path.splitext(walkfilename)[0] + newext
                print "--> Converting: " + pathtoout + "/" + walkfilename
                print "----------> To: " + pathtoout + "/" + htmlfilename
                bms = "multimarkdown --batch --process-html --smart --notes --output=\""
                fms = bms + pathtoout + "/" + htmlfilename + "\" " + walkfilename
                os.system(fms)
                os.remove(walkfilename)
    toc = timeit.default_timer()
    tt = toc - tic
    print ""
    print OKBLUE + "Build Complete" + ENDC
    print ("Time taken: %.3f seconds" % tt)
    print ""
    return
#
def servesite():
    stic = timeit.default_timer()
    print OKBLUE + "Stand By..." + ENDC
    if not os.path.exists("out"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be built!"
        print ""
        exit(2)
    sso = "http-server out -p 1313"
    os.system(sso)
    stoc = timeit.default_timer()
    stt = stoc - stic
    print ""
    stimemins = stt / 60
    print ("Server Uptime: %.3f minutes" % stimemins)
    return
#
def publish():
    ptic = timeit.default_timer()
    print OKBLUE + "Publishing ..." + ENDC
    print ""
    if not os.path.exists("out"):
        print FAIL + "ERROR: " + ENDC + "minion does not appear to be built"
        print ""
        exit(2)
    if not os.path.exists("pub.conf"):
        print FAIL + "ERROR: " + ENDC + "pub.conf file not found!"
        print ""
        exit(2)
    config = ConfigParser.ConfigParser()
    config.read('pub.conf')
    target = config.get('Target','directory')
    expand_target = os.path.expanduser(target)
    print "Target: " + expand_target

    if not os.path.exists(expand_target):
        print FAIL + "ERROR: " + ENDC + "Target diectory does not exist!"
        print ""
        exit(2)
    rsync_cmd = "rsync -avzhW --exclude '.*' --delete" + " out/ " + expand_target
    os.system(rsync_cmd)
    ptoc = timeit.default_timer()
    ptt = ptoc - ptic
    print ""
    print OKBLUE + "Publishing Complete" + ENDC
    print ("Time taken: %.3f seconds" % ptt)
    print ""
    return
