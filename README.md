# MINION README #

This README details the steps necessary to get minion up and running.

### What is this repository for? ###

* minion is static site generator using mulimarkdown.
* Version 1.1


### How do I get set up? ###

* install - copy the executable script `minion-1.1.py` and `minmod.pyc` to your path and rename `minion-1.1.py` as follows:

```
mv minion-1.1.py minion

```
* Configuration - create a file named `pub.conf` in your local folder specifying your target folder(folder to publish to) as follows:

```
[Target]
directory: /path/to/your/webserver/directory

```

* Dependencies:
	1. [yuicompressor](https://github.com/yui/yuicompressor/releases)
	2. [multimarkdown](http://fletcherpenney.net/multimarkdown/download)
	3. [http-server](https://www.npmjs.com/package/http-server)
	4. [base64](http://base64.sourceforge.net/)
	5. [rsync](https://rsync.samba.org/download.html)

* Documentation:
	1. `minion --init` creates the folder structure required for your project in the current folder and creates the following folders:
		* __content__ - where your `.mmd` files are stored (__NOTE:__ `.md` or `.markdown` files will not be compiled). categories are created with sub-folders.
		* __media__ - where your images, videos and other files are stored. this folder and sub-folders are copied as is to your site when published. linking to them via relative links is recommended,
		* __includes__ - where your included text files are stored.  these files are compiled into the resulting HTML files and will remain here.
		* __encodes__ - where you put your image files you wish to convert to base64 encoded text files. when encoded they are moved to your includes folder.
		* __public__ - where you put your CSS, JS and font files. Any CSS and JS files will be compressed on build and moved to the public folder in your site. The font files are moved as is to the public folder in your site.
	2.	`minion --encode` encodes any JPG, PNG and GIF files to base64 TXT file and moved to the includes folder. As each image is converted you are are asked for a caption for the image.
	3. `minion --build` builds your site. The resulting website will be stored in the out folder.
	4.	`minion --serve` this will start a web server on port __1313__ so you can check your site prior to publishing.
	5.	`minion --publish` uploads you site to the folder specified in the `pub.conf` file in your local folder as specified above in the configuration section.

### Who do I talk to? ###

* [Kirk Dawson](https://bitbucket.org/kdawson133)
